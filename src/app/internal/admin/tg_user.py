from django.contrib import admin

from app.internal.models.tg_user import TgUser


@admin.register(TgUser)
class AdminUserTgUser(admin.ModelAdmin):
    list_display = ('username', 'phone')
    pass
