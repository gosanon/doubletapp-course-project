from app.internal.transport.bot.utils import requires_user_phone, get_username
from app.internal.services.user_service import read_tg_user, update_tg_user_phone


def start_handler(update, ctx):
	update.message.reply_text(
		'Commands: /start, /help, /set_phone <phone>, /me.\n'
		'\nWARNING! It is unsafe to set your own phone number or any other sensitive - anyone with your username will be able to see it.')


def help_handler(update, ctx):
	update.message.reply_text(
		'''Available commands:

/start
Show message which appears on bot's first start

/set_phone phone
Save some phone to the db (in international format, starts with "+"). It can later be read by you with /me or by anyone at <code>/me</code> endpoint with your username.

/me
Display the data related to your account by username.

WARNING! It is unsafe to set your own phone number or any other sensitive information - anyone with your username will be able to see it.'''
		, parse_mode='HTML')


# This decorator makes a request to db every time.
# Maybe there is a better way to make this checkup faster;
# For example, every message may have some sign that user hasn't set up the phone yet,
# But this would still need a fallback if db wipes.
# Anyway, that's the best thing I could've come up with.
@requires_user_phone
def me_handler(update, ctx):
	username = get_username(update)
	user_data = read_tg_user(username)

	update.message.reply_text(
		f'Username: {user_data.username}\n'
		f'Phone number: {user_data.phone}')


# This should be a MessageHandler to prevent args-like parsing
def set_phone_handler(update, ctx):
	username = get_username(update)
	target_phone_number = update.message.text.replace('/set_phone ', '')
	action_result_status = update_tg_user_phone(username, target_phone_number)

	update.message.reply_text({
		'Success': 'Phone has been updated!',
		'Invalid phone number format': (
			'Invalid phone number format.\n'
			'Should start with a "+" and be a valid phone number.')
	}[action_result_status])
