from app.internal.services.user_service import create_tg_user, read_tg_user


def get_username(update):
	return update.message.from_user['username']


def requires_user_phone(handler):
	def wrapper(update, ctx):
		username = get_username(update)
		user_data = read_tg_user(username)

		if not user_data:
			create_tg_user(username)
			update.message.reply_text('Please, enter phone number first, use /set_phone (you were not registered)')
			return
		
		if not user_data.phone:
			update.message.reply_text('Please, enter phone number first, use /set_phone')
			return

		return handler(update, ctx)
	
	return wrapper