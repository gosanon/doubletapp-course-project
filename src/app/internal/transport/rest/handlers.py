from app.internal.services.user_service import read_tg_user
from app.internal.transport.rest.utils import reply_failure
from app.internal.transport.rest.utils import reply_success


def me_handler(request):
	username = request.GET.get('username', None)
	if not username:
		return reply_failure('Username not provided')
	
	user_data = read_tg_user(username)

	if not user_data:
		return reply_failure('No data for this username')

	return reply_success(phone=str(user_data.phone))