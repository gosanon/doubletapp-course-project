from django.http import JsonResponse


def reply_success(data={}, **kwargs):
	return JsonResponse(dict(data, success=True, **kwargs))


def reply_failure(message=''):
	return JsonResponse(dict(message=message, success=False))