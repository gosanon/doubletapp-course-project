from phonenumbers import NumberParseException, is_valid_number, parse
from app.models import TgUser


def create_tg_user(username):
	TgUser(username=username).save()
	return 'Success'


def read_tg_user(username):
	user = TgUser.objects.filter(username=username).first()
	return user


def update_tg_user_phone(username, phone):
	is_phone_valid = False
	try:
		is_phone_valid = is_valid_number(parse(phone, None))
	except NumberParseException:
		pass

	if not is_phone_valid:
		return 'Invalid phone number format'

	TgUser.objects.update_or_create(username=username, defaults=dict(phone=phone))
	return 'Success'
