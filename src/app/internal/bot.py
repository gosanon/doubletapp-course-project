from django.conf import settings
from telegram.ext import Updater, MessageHandler, CommandHandler, Filters

from app.internal.transport.bot.handlers import start_handler, help_handler, me_handler, set_phone_handler

updater = Updater(settings.TG_BOT_TOKEN)

updater.dispatcher.add_handler(CommandHandler('start', start_handler))
updater.dispatcher.add_handler(CommandHandler('help', help_handler))
updater.dispatcher.add_handler(MessageHandler(Filters.regex('^/set_phone'), set_phone_handler))
updater.dispatcher.add_handler(CommandHandler('me', me_handler))

def start_bot():
	updater.start_polling()
	print('Bot is up and running!')
	updater.idle()