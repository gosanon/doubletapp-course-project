from django.urls import path

from app.internal.transport.rest.handlers import me_handler


urlpatterns = [
	path('me', me_handler)
]
