from django.db import models
from django.core.validators import MinLengthValidator
from django.db.models import CharField
from phonenumber_field.modelfields import PhoneNumberField

class TgUser(models.Model):
	username = CharField(max_length=32, validators=[MinLengthValidator(5)])
	phone = PhoneNumberField(blank=True, help_text='Some phone number.')

	def __str__(self):
		return '{' + f'"{self.username}": "{self.phone}"' + '}'
