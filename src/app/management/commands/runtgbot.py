from django.core.management.base import BaseCommand

from app.internal.bot import start_bot


class Command(BaseCommand):
	help = 'Starts telegram bot from app/internal/bot.py'

	def handle(self, *args, **kwargs):
		print('Starting bot...')
		start_bot()
