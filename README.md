## Setup

```
pipenv install
```

## Run

Dev server:
```
make dev
```

Telegram bot:
```
make tgbot
```